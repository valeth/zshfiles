#!/bin/bash

if [[ -z $__SCRIPT_SOURCE ]]; then
    __SCRIPT_SOURCE=1

    source "$SCRIPTSDIR/debug.sh"
    source "$SCRIPTSDIR/check.sh"
    source "$SCRIPTSDIR/files.sh"

    source_scripts()
    {
        local scriptsdir="$1"
        local ext="${2:-sh}"

        check_dir_exists_create "$scriptsdir"

        msg_debug "$(co_str "lgreen" "==>") sourcing scripts from: $scriptsdir"
        for script in $(find "$scriptsdir" -maxdepth 2 -type f -regex ".*$ext"); do
            local current_dir="$(file_dir $script)"
            msg_debug "  $(co_str "green" "-->") sourcing: $script"
            [[ -r $script ]] && source "$script"
        done
        msg_debug "$(co_str "lgreen" "==>") done: $scriptsdir"
    }

fi # __SCRIPT_SOURCE
