#!/bin/bash

if [[ -z $__SCRIPT_CHECK ]]; then
    __SCRIPT_CHECK=1

    check_running()
    {
        local pid="$1"

        kill -0 "$pid" 2>/dev/null && return 0 || return 1
    }

    check_file_exists()
    {
        local file="$1"

        [[ -e "$file" ]] && return 0 || return 1
    }

    check_dir_exists_create()
    {
        local dir="$1"

        check_file_exists "$dir" || mkdir -p "$dir"
    }

    check_var_empty()
    {
        [[ -z "$1" ]] && return 0 || return 1
    }

    check_tty()
    {
        [[ $(tty) =~ /dev/tty[0-9]* ]] && return 0 || return 1
    }

    check_prog_exists()
    {
        type -p "$1" >&- && return 0 || return 1
    }

    check_prog_exists_setvar()
    {
        {
            {
                [[ -n ${__APP[$1]} ]]
            } || {
                check_prog_exists "$1" && __APP[$1]=1
            }
        } && return 0 || return 1
    }

fi # __SCRIPT_CHECK
