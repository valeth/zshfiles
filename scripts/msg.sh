#!/bin/bash

if [[ -z $__SCRIPT_MSG ]]; then
    __SCRIPT_MSG=1

    # print a string in a specific color
    co_str()
    {
        local co=""
        local str="$2"

        case ${1} in
            none)     co='\e[0m'    ;;
            black)    co='\e[0;30m' ;;
            red)      co='\e[0;31m' ;;
            green)    co='\e[0;32m' ;;
            brown)    co='\e[0;33m' ;;
            blue)     co='\e[0;34m' ;;
            purple)   co='\e[0;35m' ;;
            cyan)     co='\e[0;36m' ;;
            l*grey)   co='\e[0;37m' ;;
            d*grey)   co='\e[1;30m' ;;
            l*red)    co='\e[1;31m' ;;
            l*green)  co='\e[1;32m' ;;
            yellow)   co='\e[1;33m' ;;
            l*blue)   co='\e[1;34m' ;;
            l*purple) co='\e[1;35m' ;;
            l*cyan)   co='\e[1;36m' ;;
            white)    co='\e[1;37m' ;;
            *)        co="${1}"     ;;
        esac

        echo "${co}${str}\e[0m"
    }

    # print a message to stdout or stderr
    msg_print()
    {
        local out="$1"
        local msg="$2"

        case ${out} in
            stderr) printf "${msg}\n" 1>&2 ;;
            stdout) printf "${msg}\n"      ;;
        esac
    }

    # send message as dbus notification
    # TODO: not working, look for alternative
    msg_dbus_notify()
    {
        [[ ${1} == "stderr" ]]   && \
            local urgency="critical" || \
            local urgency="normal"
        local body="${2}"
        local summary="${3:- }"

        local app_name="${4:-}"
        local icon="${5:-}"
        local category="${6:-}"
        local expire="${7:-1}"

        notify-send -i "${icon}" -a "${app_name}" -c "${category}" -u "${urgency}" \
                    -t "${expire}" "${summary}" "${body}" &>/dev/null
    }


    # print message and display a dbus notification
    msg_notify()
    {
        local out="$1"
        local msg="$2"

        msg_print "${out}" "${msg}"
        msg_dbus_notify "${out}" "${msg}"
    }

fi # __SCRIPT_MSG
