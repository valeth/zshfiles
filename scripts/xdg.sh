#!/bin/bash

if [[ -z $__SCRIPT_XDG ]]; then
    __SCRIPT_XDG=1
    
    source "$SCRIPTSDIR/check.sh"

    config_dir()
    {
        local dir=""

        if check_var_empty "$XDG_CONFIG_HOME"; then
            dir="$HOME/.config/$1"
        else
            dir="$XDG_CONFIG_HOME/$1"
        fi

        check_dir_exists_create "$dir"
        echo "$dir"
    }

    data_dir()
    {
        local dir=""

        if check_var_empty "$XDG_DATA_HOME"; then
            dir="$HOME/.local/share/$1"
        else
            dir="$XDG_DATA_HOME/$1"
        fi

        check_dir_exists_create "$dir"
        echo "$dir"
    }

    cache_dir()
    {
        local dir=""

        if check_var_empty "$XDG_CACHE_HOME"; then
            dir="$HOME/.cache/$1"
        else
            dir="$XDG_CACHE_HOME/$1"
        fi

        check_dir_exists_create "$dir"
        echo "$dir"
    }

    runtime_dir()
    {
        local dir=""

        if check_var_empty "$XDG_RUNTIME_DIR"; then
            dir="$(mktemp -d ${1}-XXXXXXXX)" 
        else
            dir="$XDG_RUNTIME_DIR/$1"
        fi
        
        check_dir_exists_create "$dir"
        echo "$dir"
    }


    bin_dir()
    {
        local dir=""

        if check_var_empty "$XDG_BIN_HOME"; then
            dir="$HOME/.local/bin"
        else
            dir="$XDG_BIN_HOME"
        fi

        check_dir_exists_create "$dir"
        echo "$dir"
    }

fi # __SCRIPT_XDG
