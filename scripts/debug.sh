#!/bin/bash

if [[ -z $__SCRIPT_DEBUG ]]; then
    __SCRIPT_DEBUG=1

    source "$SCRIPTSDIR/msg.sh"

    msg_debug()
    {
        local msg="$1"
        (( $__SHELL_NDEBUG )) || msg_print "stderr" "$(co_str "green" "[DEBUG]") $msg"
    }

    msg_err()
    {
        local msg="$1"
        msg_print "stderr" "$(co_str "red" "[ERROR]") $msg"
    }

    msg_warn()
    {
        local msg="$1"
        msg_print "stderr" "$(co_str "lgreen" "[WARN ]") $msg"
    }

    msg_info()
    {
        local msg="$1"
        msg_print "stdout" "$(co_str "yellow" "[INFO ]") $msg"
    }

fi # __SCRIPT_DEBUG
