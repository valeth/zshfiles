#!/usr/bin/zsh

source ${ZDOTDIR}/scripts/debug.sh

_modulespath="${ZDOTDIR}/modules"

function is_interactive_module {
	if [[ $(cat ${_modulespath}/$1/interactive) == "true" ]]; then
		return 0
	else
		return 1
	fi
}

function is_login_module {
	if [[ $(cat ${_modulespath}/$1/login) == "true" ]]; then
		return 0
	else
		return 1
	fi
}

function load_interactive() {
	for module in $@; do
		if is_interactive_module "${module}"; then
			source "${_modulespath}/${module}/init.zsh"
		else
			msg_err "${module} is not an interactive module"
		fi
	done
}

function load_login() {
	for module in $@; do
		if is_login_module "${module}"; then
			source "${_modulespath}/${module}/init.zsh"
		else
			log_error "${module} is not a login module"
		fi
	done
}
