#!/bin/bash

if [[ -z $__SCRIPT_FILES ]]; then
    __SCRIPT_FILES=1

    file_ext()
    {
        echo "${1##*.}";
    }

    file_dir()
    {
        echo "${1%/*}";
    }

    file_name()
    {
        echo "${1##*/}";
    }

    file_name_noext()
    {
        local file="${1%%.*}";
        echo "${file##*/}";
    }


    file_type()
    {
        file -b "${1}";
    }

    file_mime()
    {
        file -b --mime-type "${1}";
    }

fi # __SCRIPT_FILES
