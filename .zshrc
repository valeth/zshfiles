#!/usr/bin/zsh

msg_debug "$(co_str "lgreen" "==>") sourcing: $ZDOTDIR/.zshrc"

#export HOME="/home/$USER"

source ${ZDOTDIR}/modules/load.zsh

source_scripts "$ZDOTDIR/zshrc.d"

msg_debug "$(co_str "lgreen" "==>") done: $ZDOTDIR/.zshrc"
