#!/usr/bin/bash

if which ls >&-; then

alias ls='ls -C --classify --color=auto --group-directories-first --sort=extension'
alias ll='ls -l --human-readable'
alias la='ls -l --human-readable --almost-all'
alias dfh='df --human-readable'
alias duh='du --summarize --total --human-readable'
alias cp='cp --verbose --interactive'
alias ln='ln --verbose --interactive'
alias rmdir='rmdir --verbose'
alias mkdir='mkdir --verbose --parents'
alias shred='shred --verbose'

lscat()
{
	if [[ -n "$1" ]]; then
		local name="$1"
	else
		local name="$PWD"
	fi

	local encoding="$(file --dereference --brief --mime-encoding $name)"

	if [[ -f "$name" && "$encoding" != "binary" ]]; then
		cat "$name"
	elif [[ -d "$name" ]]; then
		ls -lhF --color=auto --group-directories-first --sort=extension "$name"
	else
		file --mime-type "$name"
	fi
}

alias show="lscat"

fi
