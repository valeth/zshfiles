#!/usr/bin/zsh

# changing directories
setopt auto_cd
setopt auto_pushd
setopt pushd_ignore_dups

# completion
setopt always_to_end
setopt complete_in_word

# globbing
setopt extended_glob
setopt no_nomatch

# history
setopt extended_history
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt share_history

# job control
setopt auto_resume
setopt nohup
setopt long_list_jobs
setopt notify

# shell emulation
setopt no_sh_word_split

# zle
setopt no_beep
setopt emacs
