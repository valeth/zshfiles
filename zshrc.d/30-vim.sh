nvim()
{
  unset VIMINIT
  /usr/bin/nvim -N "$@"
}

vman()
{
  vim <(man "$@")
}

confirm_exit()
{
  printf "%s\n%s" "Exit shell?" "(y)es, (n)o: "

  while read line; do
    case $line in
      y|Y) exit ;;
      n|N) return ;;
    esac
  done
}

export EDITOR='nvim'

alias vim="${EDITOR}"
alias vi="${EDITOR}"
alias xvi="${EDITOR} -c ':%!xxd'"
alias edit="${EDITOR}"
alias ed="${EDITOR}"

alias svim='sudo -e'
alias svi='svim'

alias :q="confirm_exit"
