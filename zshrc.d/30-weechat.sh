#!/usr/bin/env bash

if which weechat-curses >&-; then

alias weechat-curses="weechat-curses -d $(config_dir weechat)"
alias weechat="weechat-curses"
alias irc='weechat-curses'

fi
