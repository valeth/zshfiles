open() {
  if ! which xdg-open >&-; then
    return -1
  fi

  if [[ -z $1 ]]; then
    xdg-open ./ &>/dev/null
  else
    for file in "$@"; do
      xdg-open "$file" &>/dev/null
    done
  fi

  return 0
}
