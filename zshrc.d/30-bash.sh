#!/usr/bin/bash

if which bash >&-; then

alias bash="bash --rcfile $(config_dir bash)/bashrc"

fi
