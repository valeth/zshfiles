#!/usr/bin/bash

if which systemctl >&-; then

alias userctl='/usr/bin/systemctl --user'

alias bye='/usr/bin/systemctl suspend; exit'

fi
