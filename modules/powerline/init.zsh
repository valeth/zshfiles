#!/usr/bin/zsh

autoload -U promptinit
promptinit

_powerline_path="/usr/share/zsh/site-contrib/powerline.zsh"
_default_prompt="adam2"

if type powerline >&-; then
	# don't use the powerline prompt in a TTY
	if ([[ $TERM != "linux" ]] && [[ -z $ZSH_PROMPT ]]); then
		# make sure no other prompt is enabled
		prompt off
		if [[ -f "${_powerline_path}"  ]]; then
			source "${_powerline_path}"
		fi
	else
		prompt "${_default_prompt}"
	fi
fi

unset _powerline_path
unset _default_prompt
