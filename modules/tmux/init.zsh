#!/usr/bin/zsh

if type tmux >&-; then
	alias tmux="tmux -2 -f $TMUX_CONFIG"
	alias tmux-config="$EDITOR $TMUX_CONFIG"
fi
