source ${ZDOTDIR}/scripts/modules.zsh

load_login 'tmux'

load_interactive 'powerline' 'syntax-highlighting'
