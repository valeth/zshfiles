#!/usr/bin/env zsh


export PATH="/usr/lib/ccache/bin:$(bin_dir):$(bin_dir)/wrappers:$(bin_dir)/cabal:$SCRIPTSDIR:$PATH"


msg_debug "$(co_str "lgreen" "==>") sourcing: $ZDOTDIR/.zprofile"

source_scripts "$ZDOTDIR/zprofile.d"

msg_debug "$(co_str "lgreen" "==>") done: $ZDOTDIR/.zprofile"


#[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
