#!/usr/bin/zsh

# source helper scripts
source "$SCRIPTSDIR/__init__"


msg_debug "$(co_str "lgreen" "==>") sourcing: $ZDOTDIR/.zshenv"

export HISTSIZE=5000
export SAVEHIST=10000
export HISTFILE="$(cache_dir zsh)/history"

source_scripts "$ZDOTDIR/zshenv.d"

msg_debug "$(co_str "lgreen" "==>") done: $ZDOTDIR/.zshenv"
